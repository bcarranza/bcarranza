:wave: Hello, world! My name is **Brie** Carranza. I am a **Staff Support Engineer** at GitLab. I joined GitLab in May 2020, just after [GitLab 13.0](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/) was released. :fox: :purple_heart: 

I spend my time working to understand the cause(s) of unexpected behavior and helping folks make the best use of GitLab. I fell in love with GitLab as a product before joining the team so it's super fun and exciting to get to help others experience how cool and fun it can be! You can learn a bit more about me in my [README](https://bcarranza.gitlab.io/readme/), which is in need of an update as of this writing. You can read about my [first year at GitLab](https://brie.dev/year-at-gitlab/). 

I really enjoy :cat: emoji :cactus: and experimenting with and learning about interesting and creative uses of emoji. 

🛡️ I have an academic and professional background in cybersecurity.

## :pear: Pairing

  I really enjoy pairing and can find it to be a very effective way to troubleshoot a problem and build a path to resolution. I also really appreciate the opportunity to poke through logs, build a timeline, reproduce the problem, dive through source code on my own. 

  - :calendar: Feel free to put a pairing session on my calendar.
    - Please feel free to let me know what you'd like to look like in advance so I can prepare as time permits. 

## :school_satchel: How does that work?

I really enjoy trying out new features and tools. Pop a note in [#spt_today_i_learned](https://gitlab.slack.com/archives/C01V41Z2NAU) (assuming you are a GitLab team member :smile:) with your cool finds!

In December 2020, I did a self-led "30 Days of GraphQL" effort and have continued to be interested in GraphQL and how it can be used to solve problems. My notes and code samples are in [this project](https://gitlab.com/brie/graphql). I've been interested in GraphQL ever since. 

## :globe_with_meridians: Around the Internet

  - ✨ I wrote about [Batman and troubleshooting](https://brie.dev/troubleshooting/). 

You can read more in my [README](https://bcarranza.gitlab.io/readme/) which has `0` emoji. :cry:

On my personal GitLab profile (`@brie`):

- 🍓 [awesome-ldap](https://gitlab.com/brie/awesome-ldap)
- 🗑 [faux-credential-dump-generator](https://gitlab.com/brie/faux-credential-dump-generator) - Generate a fake credential dump, for testing purposes.
- ⭐ [graphql](https://gitlab.com/brie/graphql) - Notes and scripts and stuff as I learn more about GraphQL.
- 📗 [pipeline-cleaner](https://gitlab.com/brie/pipeline-cleaner) - A Python project that deletes all skipped/failed/canceled pipelines, leaving behind only green.

Elsewhere:

- 🧀 [brie.dev](https://brie.dev/)
- 🌻 [sunflower.gallery](https://sunflower.gallery/)
- 🐘 [@brie@infosec.exchange](https://infosec.exchange/@brie)
- 🐱 [git cute commit messages](https://gitcute.cat/) 
- 🐦 [@whoamibrie](https://twitter.com/whoamibrie)
- 🌟 [awesome-kinesis](https://github.com/bbbbbrie/awesome-kinesis)
